package api.SO;

import io.swagger.annotations.ApiModelProperty;

/**
 * sentence table
 */
public class SentenceSO {


    @ApiModelProperty(value = "id of the sentence")
    private long id;

    @ApiModelProperty(value = "sentence that cannot contain \"no\"", example = "no no and one more no")
    private String sentence;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }
}
