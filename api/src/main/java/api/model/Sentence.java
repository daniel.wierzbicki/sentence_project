package api.model;

import com.sun.istack.NotNull;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * sentence table
 */
@Entity
public class Sentence {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    @ApiModelProperty(value = "id of the sentence")
    private long id;
    @NotNull
    @Column(name="updated_sentence")
    @ApiModelProperty(value = "sentence that cannot contain \"no\"", example = "no no and one more no")
    private String sentence;

    public Sentence(String sentence) {
        this.sentence = sentence;
    }

    public Sentence() {

    }

    public long getId() {
        return id;
    }

    public Sentence(long id, String sentence) {
        this.id = id;
        this.sentence = sentence;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }
}
