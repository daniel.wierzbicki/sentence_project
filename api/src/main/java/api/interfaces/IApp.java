package api.interfaces;

import api.SO.SentenceSO;
import api.model.Sentence;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * interface
 */
@RequestMapping(IApp.Constants.API_NAME)
@Api(tags={IApp.Constants.API_NAME})
public interface IApp {

    /**
     * set the API_NAME
     */
    final class Constants {
        public static final String API_NAME = "/word/removal/v1";
    }

    /**
     * request for giving sentence
     * @param sentenceSO example sentence
     * @return sentences without "no"
     */
    @PostMapping("/sentence")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value="This API removes \"no\" word from given sentences")
    @ApiResponses({
            @ApiResponse(code=200, message="Successfully removed no word"),
            @ApiResponse(code=404, message="\"no\" word not found"),
            @ApiResponse(code=400, message="Sentence is empty")

    })
    public Sentence removeNo(@RequestBody SentenceSO sentenceSO);

}
