package app.controller;

import api.SO.SentenceSO;
import api.interfaces.IApp;
import api.model.Sentence;
import app.converter.SentenceConverter;
import app.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Lorem ipsum {@link api.interfaces.IApp}
 */
@RestController
public class AppController implements IApp {

    @Autowired
    private AppService appService;
    private SentenceConverter sentenceConverter;

    public AppController(AppService appService, SentenceConverter sentenceConverter) {
        this.appService = appService;
        this.sentenceConverter = sentenceConverter;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public Sentence removeNo(@RequestBody SentenceSO sentenceSO) {

        return appService.removeNo(sentenceSO);
    }

}


