package app.converter;

import api.SO.SentenceSO;
import api.model.Sentence;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class SentenceConverter {

    public SentenceSO toSO(Sentence sentence) {
        SentenceSO sentenceSO = new SentenceSO();
        BeanUtils.copyProperties(sentence, sentenceSO);
        return sentenceSO;
    }

    public Sentence toEntity(SentenceSO sentenceSO) {
        Sentence sentence = new Sentence();
        BeanUtils.copyProperties(sentenceSO, sentence);
        return sentence;
    }

}





