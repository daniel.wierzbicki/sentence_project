package app.service;

import api.SO.SentenceSO;
import api.model.Sentence;
import app.converter.SentenceConverter;
import app.repository.SentenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * service
 */
@Service
public class AppService {

    @Autowired
    private final WordRemovalUtil wordRemovalUtil;
    private final SentenceRepository sentenceRepository;
    private final SentenceConverter sentenceConverter;

    /**
     * constructor
     * @param wordRemovalUtil object of another class
     * @param sentenceRepository repository for saving data
     * @param sentenceConverter converts DTO to Entity
     */
    public AppService(WordRemovalUtil wordRemovalUtil, SentenceRepository sentenceRepository, SentenceConverter sentenceConverter) {
        this.wordRemovalUtil = wordRemovalUtil;
        this.sentenceRepository = sentenceRepository;
        this.sentenceConverter = sentenceConverter;
    }


    /**
     * saves data to the repository
     * @param sentenceSO gives sentence
     * @return the saved new sentence without "no"
     */
    public Sentence removeNo(SentenceSO sentenceSO) {

        return sentenceRepository.save(wordRemovalUtil.removeNo(sentenceConverter.toEntity(sentenceSO)));
    }

}
