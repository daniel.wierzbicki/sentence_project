package app.service;

import api.SO.SentenceSO;
import api.model.Sentence;
import app.converter.SentenceConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

/**
 * removes "no" from given sentence
 */

@Component
public class WordRemovalUtil {
    private static final String WORD_TO_REMOVE = "no";

    @Autowired
    private final SentenceConverter sentenceConverter;

    public WordRemovalUtil(SentenceConverter sentenceConverter) {
        this.sentenceConverter = sentenceConverter;
    }

    /**
     * check if sentence has "no" and deletes it
     * @param sentence given sentence
     * @return updated sentence and HttpStatus
     */
    public Sentence removeNo(Sentence sentence){
        if(sentence.getSentence().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Sentence cannot be empty");
        }
       if(!sentence.getSentence().contains(WORD_TO_REMOVE)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No in sentence not found");
       }
        sentence.setSentence(sentence.getSentence().replaceAll(WORD_TO_REMOVE, ""));

       return sentence;
    }

}
